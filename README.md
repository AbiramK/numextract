# NumExtract

[![PyPI version](https://badge.fury.io/py/numextract.svg)](https://badge.fury.io/py/numextract)

>Extracts numbers into a list from a text file.

## Install
```
$ pip install numextract
```

## Usage

<img src="https://gitlab.com/AbiramK/numextract/raw/master/assets/usage.png" width="400">

## License

[GNU][License]

[LICENSE]: https://www.gnu.org/licenses/gpl-3.0.en.html